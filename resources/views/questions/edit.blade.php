@extends('layouts.app')

@section('title')
    Sửa câu hỏi
@endsection

@section('content')
<div class="container">

    <div class="row justify-content-center">
        <div class="col-md-8 col-md-offset-2">
            @include('flash::message')
            @if ($errors->any())
                <div class="alert alert-danger alert-dismissible fade in" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="card">
                <div class="card-header">
                    Sửa câu hỏi
                </div>

                <div class="card-body">
                    <form class="form-horizontal" method="POST" action="{{ route('questions.update', ['user' =>
                    $question->id]) }}">
                        {{ csrf_field() }} {{ method_field('PUT') }}

                        <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                            <label for="title" class="control-label">Câu hỏi:</label>

                            {{--<div class="col-md-6">--}}
                                <textarea id="title" rows="3" class="form-control" name="title" required>{{ old('title',
                                $question->title)
                                }}</textarea>
                                @if ($errors->has('title'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                @endif
                            {{--</div>--}}
                        </div>

                        <div class="form-group">
                            <label for="code_id" class="control-label">John Holland Code</label>

                            {{--<div class="col-md-3">--}}
                                @if(!$codes->isEmpty())
                                <select name="code_id" class="form-control">
                                    @foreach ($codes as $key => $code)
                                        <option value="{{ $code->id }}" {{ old('code_id', $question->code_id) ==
                                        $code->id ?
                                        'selected' :
                                         ''
                                        }}>{{ $code->title }}</option>
                                    @endforeach
                                </select>
                                @endif
                            {{--</div>--}}
                        </div>

                        <div class="form-group">
                            {{--<div class="col-md-6 col-md-offset-4">--}}
                                <button type="submit" class="btn btn-primary">
                                    Cập nhật
                                </button>

                                <a href="{{ route('questions.index') }}" class="btn btn-danger btn-xs pull-right">
                                    Trở về
                                </a>
                            {{--</div>--}}
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
