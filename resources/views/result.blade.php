@extends('layouts.app')

@section('title')
    Kết quả trắc nghiệm
@endsection

@section('content')
    {{--{{ dd($first) }}--}}
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">

                <div class="card">
                    <div class="card-body">
                        <h4>Kết quả trắc nghiệm:</h4>
                        <?php
                        $dataMajors = [];
                        if (!empty($first['code']->majors)) {
                            $dataMajors = array_merge($first['code']->majors->toArray(), $second['code']->majors->toArray());
                        }

                        $dataMajors = collect($dataMajors);
                        $plucked = $dataMajors->pluck('id');

                        $majorIds = [];
                        $str = '';
//                        dd ($chosenMajors);
                        foreach($majors as $key => $major) {
                            $majorIds[] = $major->id;
                            $str .= $major->title;
                            if ($key == 0) $str .= ' & ';
                        }

                        $intersect = $plucked->intersect($majorIds);
                        ?>

                        <div class="row">
                            <div class="col-md-12">
                                @if($intersect->isEmpty())
                                    <p class="text-danger">Kết quả không phù hợp với lựa chọn ngành bạn dự định học
                                        ({{ $str }})
                                    </p>
                                @endif
                            </div>
                            <div class="col-md-8">
                                <table class="table table-bordered table-sm">
                                    <thead>
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">Nhóm tính cách</th>
                                        <th scope="col">Điểm</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($results as $key => $result)
                                        <tr>
                                            <th scope="row">{{ $key + 1 }}</th>
                                            <td>{{ $result['code']->title }}</td>
                                            <td>{{ $result['total'] }}</td>
                                        </tr>
                                        {{--                            <p><strong>{{ $result['code']['title'] }}:</strong> {{ $result['total'] }} điểm</p>--}}
                                    @endforeach

                                    </tbody>
                                </table>
                            </div>
                        </div>


                        <h4>Vậy bạn thuộc 1 trong 2 kiểu tính cách sau:</h4>

                        <h5>1/ {{ $first['code']->title }}</h5>
                        <p>{{ $first['code']->description }}</p>
                        <p><strong>Các ngành học phù hợp:</strong></p>

                        <ul>
                            @foreach($first['code']->majors as $major)
                                <li>{{ $major->title }}</li>
                            @endforeach
                        </ul>


                        <h5>2/ {{ $second['code']->title }}</h5>
                        <p>{{ $second['code']->description }}</p>
                        <p><strong>Các ngành học phù hợp:</strong></p>

                        <ul>
                            @foreach($second['code']->majors as $major)
                                <li>{{ $major->title }}</li>
                            @endforeach
                        </ul>

                        <div class="form-group">
                            <a href="{{ route('home') }}" class="btn btn-danger btn-xs btn-block">
                                Làm lại
                            </a>
                            {{--</div>--}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
