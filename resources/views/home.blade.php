@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">

            @include('flash::message')
            @if ($errors->any())
                <div class="card">
                    <div class="card-body">
                        <div class="alert alert-danger alert-dismissible fade in" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            @endif



            @if(!$questions->isEmpty())
                        <form action="{{ route('home') }}" method="POST">
                            {{ csrf_field() }}

                        @foreach($questions as $code_id => $grouped)
                            @php
                                $code = $codes->get($code_id);
                            @endphp
                            <h5>Nhóm sở thích {{ $code->title }}</h5>

                                <div class="card" style="margin: 20px 0">
                                    <div class="card-body">
                                        <div class="row">
                                        @foreach($grouped as $key => $question)
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <p><strong>{{ $key +1  }}. {{ $question->title }}</strong></p>
                                                <div>
                                                @foreach($options as $point => $option)
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="radio"
                                                               name="answer[{{ $question->code_id }}][{{
                                                               $question->id }}]"
                                                               id="{{ 'answer'. $question->code_id . $question->id . $point }}"
                                                               value="{{ $point }}" oninvalid="this
                                                               .setCustomValidity('Bạn chưa trả lời')"
                                                               onvalid="this.setCustomValidity('')" required {{
                                                               (!empty
                                                               ($prevdata['results'][$code_id-1]['items'][$question->id])
                                                               &&
                                                               $prevdata['results'][$code_id-1]['items'][$question->id] == $point) || (empty
                                                               ($prevdata['results'][$code_id-1]['items'][$question->id]) && $point == 1) ?
                                                                'checked' : ''
                                                                }}>
                                                        <label class="form-check-label" for="{{ 'answer'. $question->code_id .
                                                        $question->id . $point }}">
                                                            {{ $option }}
                                                        </label>
                                                    </div>
                                                @endforeach
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        @endforeach

                            <div class="form-group">
                                {{--<div class="col-md-6 col-md-offset-4">--}}
                                <button type="submit" class="btn btn-primary btn-block">
                                    Lấy kết quả
                                </button>

                                {{--<a href="{{ route('home') }}" class="btn btn-danger btn-xs pull-right">--}}
                                    {{--Làm lại--}}
                                {{--</a>--}}
                                {{--</div>--}}
                            </div>
                        </form>
                    @endif


                {{--</div>--}}
            {{--</div>--}}
        </div>
    </div>
</div>
@endsection
