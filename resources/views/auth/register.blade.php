@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('Đăng ký') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="row">

                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label for="name" class="col-md-3 col-form-label text-md-right">{{ __('Họ tên ')
                                    }}</label>

                                    <div class="col-md-8">
                                        <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>

                                        @if ($errors->has('name'))
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="email" class="col-md-3 col-form-label text-md-right">{{ __('E-Mail')
                                    }}</label>

                                    <div class="col-md-8">
                                        <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                        @if ($errors->has('email'))
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="password" class="col-md-3 col-form-label text-md-right">{{ __('Mật
                                    khẩu ')
                            }}</label>

                                    <div class="col-md-8">
                                        <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                        @if ($errors->has('password'))
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="password-confirm" class="col-md-3 col-form-label text-md-right">{{ __
                                    ('Xác
                            nhận lại') }}</label>

                                    <div class="col-md-8">
                                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label for="gender" class="col-md-3 col-form-label text-md-right">{{ __('Giới tính ')
                                    }}</label>

                                    <div class="col-md-8">

                                        <div class="form-check form-check-inline" style="margin-top: 6px">
                                            <input class="form-check-input" type="radio"
                                                   name="gender"
                                                   id="gender1"
                                                   value="1" oninvalid="this
                                                               .setCustomValidity('Bạn chưa chọn giới tính ')"
                                                   onvalid="this.setCustomValidity('')" required {{ old('gender') == 1 ?
                                                                'checked' : ''
                                                                }}>
                                            <label class="form-check-label" for="gender1">
                                                Nam
                                            </label>
                                        </div>

                                        <div class="form-check form-check-inline" style="margin-top: 6px">
                                            <input class="form-check-input" type="radio"
                                                   name="gender"
                                                   id="gender0"
                                                   value="0" oninvalid="this
                                                               .setCustomValidity('Bạn chưa chọn giới tính ')"
                                                   onvalid="this.setCustomValidity('')" required {{ old('gender') == 0 ?
                                                                'checked' : ''
                                                                }}>
                                            <label class="form-check-label" for="gender0">
                                                Nữ
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="birthday" class="col-md-3 col-form-label text-md-right">{{ __('Ngày
                                    sinh ')
                                    }}</label>

                                    <div class="col-md-8">
                                        <input id="birthday" type="birthday" class="js-date-picker form-control{{
                                        $errors->has('birthday') ? ' is-invalid' : '' }}" name="birthday" value="{{ old('birthday') }}" required>

                                        @if ($errors->has('birthday'))
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('birthday') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="phone" class="col-md-3 col-form-label text-md-right">{{ __('Điện
                                    thoại ')
                            }}</label>

                                    <div class="col-md-8">
                                        <input id="phone" type="text" class="form-control{{ $errors->has('phone') ? '
                                        is-invalid' : '' }}" name="phone" value="{{ old('phone') }}">

                                        @if ($errors->has('phone'))
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="address" class="col-md-3 col-form-label text-md-right">{{ __
                                    ('Địa chỉ ') }}</label>

                                    <div class="col-md-8">
                                        <input id="address" type="text" class="form-control"
                                               name="address" value="{{ old('address') }}">
                                    </div>
                                </div>
                            </div>

                        </div>

                        <hr>

                        <p class="text-center"><strong>Hãy chọn ngành bạn dự định học </strong></p>

                        <div class="form-group row">
                            <label for="" class="col-md-3 col-form-label text-md-right">{{ __
                                    ('Ngành 1 ') }}</label>

                            <div class="col-md-8">
                                <select name="majors[0]" class="form-control" required>
                                    <option value="">Chọn ngành</option>
                                    @foreach($majors as $major)
                                        <option value="{{ $major->id }}">{{ $major->title }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="" class="col-md-3 col-form-label text-md-right">{{ __
                                    ('Ngành 2 ') }}</label>

                            <div class="col-md-8">
                                <select name="majors[1]" class="form-control" required>
                                    <option value="">Chọn ngành</option>
                                    @foreach($majors as $major)
                                        <option value="{{ $major->id }}">{{ $major->title }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        {{--<hr>--}}
                        {{--<p class="text-center"><strong>Chọn nhóm khả năng, sở thích phù hợp nhất với bạn (Chọn từ 1-2 nhóm)--}}
                            {{--</strong></p>--}}
                        {{--<div class="row" style="padding: 0 40px">--}}
                            {{--<div class="col-md-12">--}}
                                {{--<div class="form-group row">--}}
                                    {{--@foreach($codes as $code)--}}
                                        {{--<div class="form-check">--}}
                                            {{--<input class="form-check-input" type="checkbox" value="{{ $code->id--}}
                                            {{--}}" name="codes[]"--}}
                                                   {{--id="code{{$code->id}}">--}}
                                            {{--<label class="form-check-label" for="code{{$code->id}}">--}}
                                                {{--{{ $code->description }}--}}
                                            {{--</label>--}}
                                        {{--</div>--}}
                                    {{--@endforeach--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        <hr>
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-5">
                                <button type="submit" class="btn btn-primary">
                                    {{ __(' Đăng ký') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="/js/flatpickr/dist/flatpickr.js"></script>
<script>
    flatpickr('.js-date-picker',{
        locale: 'en',
        altInput: true,
        altFormat: "d/m/Y",
        enableTime: false,
        dateFormat: "Y-m-d"
    });
</script>
@endsection
