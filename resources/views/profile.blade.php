@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('Thông tin cá nhân') }}</div>

                <div class="card-body">
                    {{--<form method="POST" action="{{ route('register') }}">--}}
                        @csrf

                        <div class="row">

                            <div class="col-md-4">
                                <div class="form-group row" style="margin-bottom: 0">
                                    <label for="name" class="col-md-3 col-form-label"><strong>{{ __('Họ
                                     tên ')
                                    }}</strong></label>

                                    {{--<div class="col-md-8">--}}
                                        <label for="name" class="col-md-8 col-form-label">{{ $user->name
                                    }}</label>

                                    {{--</div>--}}
                                </div>

                                <div class="form-group row" style="margin-bottom: 0">
                                    <label for="email" class="col-md-3 col-form-label"><strong>{{ __
                                    ('E-Mail')
                                    }}</strong></label>

                                    <label for="name" class="col-md-8 col-form-label">{{ $user->email
                                    }}</label>
                                </div>


                            </div>

                            <div class="col-md-8">
                                <div class="form-group row" style="margin-bottom: 0">
                                    <label for="gender" class="col-md-3 col-form-label"><strong>{{ __('Giới
                                    tính ')
                                    }}</strong></label>

                                    <label for="name" class="col-md-8 col-form-label">{{ $user->gender
                                    == 1 ? 'Nam' : 'Nữ '
                                    }}</label>
                                </div>

                                <div class="form-group row" style="margin-bottom: 0">
                                    <label for="birthday" class="col-md-3 col-form-label"><strong>{{ __('Ngày
                                    sinh ')
                                    }}</strong></label>

                                    <label for="name" class="col-md-8 col-form-label">{{ $user->birthday
                                    }}</label>
                                </div>

                                <div class="form-group row" style="margin-bottom: 0">
                                    <label for="phone" class="col-md-3 col-form-label"><strong>{{ __('Điện
                                    thoại ')
                            }}</strong></label>

                                    <label for="name" class="col-md-8 col-form-label">{{ $user->phone
                                    }}</label>
                                </div>

                                <div class="form-group row">
                                    <label for="address" class="col-md-3 col-form-label"><strong>{{ __
                                    ('Địa chỉ ') }}</strong></label>

                                    <label for="name" class="col-md-8 col-form-label">{{ $user->address
                                    }}</label>
                                </div>
                            </div>

                        </div>

                        <hr>

                    <div class="row">
                        <div class="col-md-12">
                            <p><strong>Ngành dự định học: </strong></p>
                            <?php
                                $dataMajors = [];
                                if (!empty($data['first']['code']['majors'])) {
                                    $dataMajors = array_merge($data['first']['code']['majors'], $data['second']['code']['majors']);
                                }

                                $dataMajors = collect($dataMajors);
                                $plucked = $dataMajors->pluck('id');
                            ?>
                            <div class="form-group row">
                                <div class="col-md-8">
                                    <ul>
                                        <?php $majorIds = []; ?>
                                        @foreach($majors as $major)
                                                <?php $majorIds[] = $major->id; ?>
                                            <li>{{ $major->title }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>

                        </div>
                        {{--<div class="col-md-8">--}}
                            {{--<p><strong>Nhóm khả năng, sở thích:--}}
                                {{--</strong></p>--}}

                            {{--<div class="row">--}}
                                {{--<div class="col-md-12">--}}
                                    {{--<div class="col-md-8">--}}
                                        {{--<ul>--}}
                                            {{--@foreach($codes as $code)--}}
                                                {{--<li>{{ $code->description }}</li>--}}
                                            {{--@endforeach--}}
                                        {{--</ul>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    </div>




                        {{--<hr>--}}
                        {{--<div class="form-group row mb-0">--}}
                            {{--<div class="col-md-6 offset-md-5">--}}
                                {{--<button type="submit" class="btn btn-primary">--}}
                                    {{--{{ __(' Đăng ký') }}--}}
                                {{--</button>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</form>--}}
                </div>
            </div>
            @if(!empty($data))
            <div class="card" style="margin-top: 20px">
                <div class="card-header">{{ __('Kết quả trắc nghiệm') }}</div>

                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <?php $intersect = $plucked->intersect($majorIds); ?>
                            @if($intersect->isEmpty())
                                <p class="text-danger">Kết quả không phù hợp với lựa chọn ngành bạn dự định học</p>
                            @endif
                        </div>

                        <div class="col-md-8">
                            <table class="table table-bordered table-sm">
                                <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Nhóm tính cách</th>
                                    <th scope="col">Điểm</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($data['results'] as $key => $result)
                                    <tr>
                                        <th scope="row">{{ $key + 1 }}</th>
                                        <td>{{ $result['code']['title'] }}</td>
                                        <td>{{ $result['total'] }}</td>
                                    </tr>
                                    {{--                            <p><strong>{{ $result['code']['title'] }}:</strong> {{ $result['total'] }} điểm</p>--}}
                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>




                    <h4>Vậy bạn thuộc 1 trong 2 kiểu tính cách sau:</h4>

                    <h5>1/ {{ $data['first']['code']['title'] }}</h5>
                    <p>{{ $data['first']['code']['description'] }}</p>
                    <p><strong>Các ngành học phù hợp:</strong></p>

                    <ul>
                        @foreach($data['first']['code']['majors'] as $major)
                            <li>{{ $major['title'] }}</li>
                        @endforeach
                    </ul>


                    <h5>2/ {{ $data['second']['code']['title'] }}</h5>
                    <p>{{ $data['second']['code']['description'] }}</p>
                    <p><strong>Các ngành học phù hợp:</strong></p>

                    <ul>
                        @foreach($data['second']['code']['majors'] as $major)
                            <li>{{ $major['title'] }}</li>
                        @endforeach
                    </ul>

                </div>
            </div>
            @endif
        </div>
    </div>
</div>
{{--<script src="/js/flatpickr/dist/flatpickr.js"></script>--}}
{{--<script>--}}
    {{--flatpickr('.js-date-picker',{--}}
        {{--locale: 'en',--}}
        {{--altInput: true,--}}
        {{--altFormat: "d/m/Y",--}}
        {{--enableTime: false,--}}
        {{--dateFormat: "Y-m-d"--}}
    {{--});--}}
{{--</script>--}}
@endsection
