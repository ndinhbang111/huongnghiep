@extends('layouts.app')

@section('title')
    Bộ câu hỏi John Holland
@endsection

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header clearfix">
                        <h5 class="float-left" style="margin: 0;line-height: 26px;">
                            Danh sách mã John Holland
                        </h5>
                        <a href="{{ route('codes.create') }}" class="btn btn-primary btn-sm float-right">
                            <span class="glyphicon glyphicon-plus-sign"></span> Tạo mới
                        </a>
                    </div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif

                            <table class="table table-condensed table-hover">
                                <thead class="thead-inverse">
                                <tr>
                                    <th>ID</th>
                                    <th>Tiêu đề</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse($codes as $code)
                                    <tr>
                                        <td scope="row">{{ $code->id }}</td>

                                        <td>
                                            {{ $code->title }}
                                        </td>

                                            <td style="text-align: center">

                                                <a class="btn btn-sm btn-primary" href="{{ route('codes.edit',
                                                $code->id) }}">
                                                    Sửa
                                                </a>

                                                <a class="btn btn-danger btn-sm" href="{{ route('codes.destroy',
                                                ['code' =>$code->id]) }}" onclick="event.preventDefault();document
                                                        .getElementById('logout-form{{$code->id}}').submit();">
                                                    Xóa
                                                </a>

                                                <form id="logout-form{{$code->id}}" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                    @csrf
                                                    {{ method_field('DELETE') }}
                                                </form>
                                            </td>

                                    </tr>
                                @empty
                                    <tr>
                                        <p class="text-danger">Không có câu hỏi nào</p>
                                    </tr>
                                @endforelse

                                </tbody>
                            </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection