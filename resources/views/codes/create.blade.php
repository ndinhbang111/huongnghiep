@extends('layouts.app')

@section('title')
    Tạo câu hỏi
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8 col-md-offset-2">
            @include('flash::message')
            @if ($errors->any())
                <div class="alert alert-danger alert-dismissible fade in" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="card">
                <div class="card-header">
                    Tạo tính cách theo John Holland
                </div>

                <div class="card-body">
                    <form class="form-horizontal" method="POST" action="{{ route('codes.store') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                            <label for="title" class="control-label">Code:</label>

                                <input id="title" value="{{ old('title')}}" class="form-control" name="title" required>

                                @if ($errors->has('title'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                @endif
                        </div>

                        <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                            <label for="description" class="control-label">Mô tả:</label>

                            <textarea id="description" rows="10" class="form-control" name="description">{{ old
                                ('description')}}</textarea>

                            @if ($errors->has('description'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                            @endif
                        </div>

                        <div class="form-group">
                            {{--<div class="col-md-6">--}}
                                <button type="submit" class="btn btn-primary">
                                    Tạo
                                </button>

                            <a href="{{ route('codes.index') }}" class="btn btn-danger btn-xs">
                                Trở về
                            </a>
                            {{--</div>--}}
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
