<?php

namespace App\Http\Controllers;

use App\Code;
use Illuminate\Http\Request;

class CodeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $codes = Code::all();
        return view('codes.index', ['codes' => $codes]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('codes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $code = new Code;
        $code->title = $request->get('title');
        $code->description = $request->get('description');
        $code->save();

        flash('Code was successfully created.')->success();

        return redirect()->route('codes.create');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Code  $code
     * @return \Illuminate\Http\Response
     */
    public function show(Code $code)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Code  $code
     * @return \Illuminate\Http\Response
     */
    public function edit(Code $code)
    {
        return view('codes.edit', ['code' => $code]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Code  $code
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Code $code)
    {
        $code->title = $request->get('title');
        $code->description = $request->get('description');
        $code->save();

        flash('Code was successfully updated.')->success();

        return redirect()->route('codes.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Code $code
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Code $code)
    {
        $code->delete();

        flash('Code was successfully deleted.')->success();

        return redirect()->route('codes.index');
    }
}
