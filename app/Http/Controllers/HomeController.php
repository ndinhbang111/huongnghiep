<?php

namespace App\Http\Controllers;

use App\Code;
use App\Major;
use App\Question;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user = Auth::user();

        $questions = Question::all()->groupBy('code_id');

        $codes = Code::all()->keyBy('id');

        $prevdata = [];

        if (!is_null($user->result)) {
            $prevdata = json_decode($user->result, true);
        }

//        dump($prevdata);
//        dd($questions);

        $options = [
            1 => 'Không đúng (1đ)',
            2 => 'Chỉ đúng trong một vài trường hợp (2đ)',
            3 => 'Chỉ đúng một nửa (3đ)',
            4 => 'Gần như đúng hầu hết mọi trường hợp (4đ)',
            5 => 'Hoàn toàn đúng (5đ)'
        ];

        return view('home', [
            'questions' => $questions,
            'options' => $options,
            'codes' => $codes,
            'prevdata' => $prevdata
        ]);
    }

    public function profile() {
        $user = Auth::user();

//        $chosenCodes = json_decode($user->chosen_codes);
        $chosenMajors = json_decode($user->chosen_majors);

        $data = [];

        if (!is_null($user->result)) {
            $data = json_decode($user->result, true);
        }

//        $codes = Code::whereIn('id', $chosenCodes)->get();
        $majors = Major::whereIn('id', $chosenMajors)->get();

        return view('profile', [
           'user' => $user,
//            'codes' => $codes,
            'majors' => $majors,
            'data' => $data
        ]);
    }

    public function result(Request $request) {

        $user = Auth::user();

        $chosenMajors = isset($user->chosen_majors) ? json_decode($user->chosen_majors) : [];

        $answers = $request->get('answer', []);

        $codes = Code::with('majors')->get()->keyBy('id');

        $results = [];

        foreach ($answers as $code => $items) {
            $total = 0;
            $countArr = [
                5 => 0,
                4 => 0,
                3 => 0,
                2 => 0,
                1 => 0
            ];
            foreach ($items as $point) {
                $total += $point;
                // dem so loai lua chon
                $countArr[$point] = $countArr[$point] + 1;
            }

            $sortNum = pow(10, 5) * $total;
            foreach ($countArr as $key => $time) {
                $sortNum += pow(10, $key - 1) * $time;
            }

            $results[] = [
                'code' => $codes->get($code),
                'total' => $total,
                'items' => $items,
                'countArr' => $countArr,
                'sortNum' => $sortNum,
            ];
        }

        $collection = collect($results);
        // sap xep theo thu tu tang dan cua tong diem theo 6 nhom
        $sorted = $collection->sortBy('sortNum');

        // lay ra nhom co tong diem max
        $first = $sorted->pop();
        $second = $sorted->pop();

        $data = ['first' => $first, 'second' => $second, 'results' => $results];

        $user->result = json_encode($data);
        $user->save();

        $data['majors'] = Major::whereIn('id', $chosenMajors)->get();

        return view('result', $data);

    }
}
