<?php

namespace App\Http\Controllers;

use App\Code;
use App\Major;
use Illuminate\Http\Request;

class MajorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $majors = Major::orderBy('id', 'desc')->get();
        return view('majors.index', ['majors' => $majors]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $codes = Code::all();
        return view('majors.create', ['codes' => $codes]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $major = new Major;
        $major->title = $request->get('title');
        $major->code_id = $request->get('code_id');
        $major->save();

        flash('Major was successfully created.')->success();

        return redirect()->route('majors.create');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Major  $major
     * @return \Illuminate\Http\Response
     */
    public function show(Major $major)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Major  $major
     * @return \Illuminate\Http\Response
     */
    public function edit(Major $major)
    {
        $codes = Code::all();
        return view('majors.edit', ['codes' => $codes, 'major' => $major]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Major  $major
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Major $major)
    {
        $major->title = $request->get('title');
        $major->code_id = $request->get('code_id');
        $major->save();

        flash('Major was successfully updated.')->success();

        return redirect()->route('majors.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Major $major
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Major $major)
    {
        $major->delete();

        flash('Major was successfully deleted.')->success();

        return redirect()->route('majors.index');
    }
}
