<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $table = 'questions';

    public function code()
    {
        return $this->belongsTo('App\Code');
    }
}
