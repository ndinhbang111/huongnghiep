<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Major extends Model
{
    protected $table = 'majors';

    public function code()
    {
        return $this->belongsTo('App\Code');
    }
}
