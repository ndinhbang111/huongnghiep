<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Code extends Model
{
    protected $table = 'codes';

    public function questions()
    {
        return $this->hasMany('App\Question');
    }

    public function majors()
    {
        return $this->hasMany('App\Major');
    }
}
