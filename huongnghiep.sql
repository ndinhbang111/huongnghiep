-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: mysql
-- Generation Time: Apr 26, 2019 at 12:05 AM
-- Server version: 5.5.62
-- PHP Version: 7.2.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `huongnghiep`
--

-- --------------------------------------------------------

--
-- Table structure for table `codes`
--

CREATE TABLE `codes` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `codes`
--

INSERT INTO `codes` (`id`, `title`, `description`, `created_at`, `updated_at`) VALUES
(1, 'R (Realistic): Thực tế', 'Có khả năng về ký thuật , công nghệ, hệ thống, thích làm việc với đồ vật, máy móc, động thực vật, thích làm các công việc ngoài trời', '2019-04-15 10:31:37', '2019-04-25 12:48:47'),
(2, 'I (Investigative): Điều tra', 'Có khả năng về quan sát, khám phá, nghiên cứu, phân tích đánh giá và giải quyết các vấn đề', '2019-04-15 10:32:32', '2019-04-25 12:49:37'),
(3, 'A (Artistic): Nghệ sĩ', 'Có khả năng về nghệ thuật, khả năng về trực giác, khả năng tưởng tượng cao, thích làm việc trong các môi trường mang tính ngẫu hứng, không khuôn mẫu', '2019-04-15 10:33:01', '2019-04-25 12:50:41'),
(4, 'S (Social): Xã hội', 'Có khả năng về ngôn ngữ, giảng giải, thích làm những việc như giảng giải, cung cấp thông tin, sự chăm sóc, giúp đỡ hoặc huấn luyện cho người khác', '2019-04-15 10:33:24', '2019-04-25 12:52:06'),
(5, 'E (Enterprising): Dám nghĩ dám làm', 'Có khả năng về kinh doanh, mạnh bạo, dám nghĩ dám làm, có thể gây ảnh hưởng, thuyết phục người khác, có khả năng quản lý', '2019-04-15 10:37:18', '2019-04-25 12:53:01'),
(6, 'C (Conventional): Tập quán', 'Có khả năng về số học, thích thực hiện những công việc chi tiết, thích làm việc với những số liệu, theo chỉ dẫn cỉa người khác hoặc các công việc văn phòng', '2019-04-15 10:37:54', '2019-04-25 12:54:13');

-- --------------------------------------------------------

--
-- Table structure for table `majors`
--

CREATE TABLE `majors` (
  `id` int(10) UNSIGNED NOT NULL,
  `code_id` int(11) DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `majors`
--

INSERT INTO `majors` (`id`, `code_id`, `title`, `created_at`, `updated_at`) VALUES
(2, 4, 'Đào tạo giáo viên', '2019-04-15 11:40:13', '2019-04-15 11:40:13'),
(3, 3, 'Nghệ thuật trình diễn', '2019-04-17 06:15:34', '2019-04-17 06:15:34'),
(4, 3, 'Mỹ thuật ứng dụng', '2019-04-17 06:15:59', '2019-04-17 06:15:59'),
(5, 4, 'Khoa học chính trị', '2019-04-17 06:17:48', '2019-04-17 06:17:48'),
(6, 4, 'Xã hội học và nhân học', '2019-04-17 06:18:00', '2019-04-17 06:18:00'),
(7, 4, 'Tâm lý học', '2019-04-17 06:18:15', '2019-04-17 06:18:15'),
(8, 5, 'Quản trị - Quản lý', '2019-04-17 06:18:48', '2019-04-17 06:18:48'),
(9, 6, 'Kế toán - Kiểm toán', '2019-04-17 06:19:06', '2019-04-17 06:19:06'),
(10, 2, 'Khoa học trái đất', '2019-04-17 06:21:15', '2019-04-17 06:21:15'),
(11, 2, 'Khoa học vật chất', '2019-04-17 06:21:23', '2019-04-17 06:21:23'),
(12, 2, 'Khoa học môi trường', '2019-04-17 06:21:40', '2019-04-17 06:21:40'),
(13, 2, 'Khoa học sự sống', '2019-04-17 06:21:49', '2019-04-17 06:21:49'),
(14, 3, 'Mỹ thuật', '2019-04-17 06:24:33', '2019-04-17 06:24:33'),
(15, 4, 'Báo chí và truyền thông', '2019-04-17 06:28:25', '2019-04-17 06:28:25'),
(16, 5, 'Kinh doanh', '2019-04-17 06:28:52', '2019-04-17 06:28:52'),
(17, 6, 'Văn thư -Lưu trữ - Bảo tàng', '2019-04-17 06:32:29', '2019-04-17 06:32:29'),
(18, 4, 'Luật', '2019-04-17 06:33:14', '2019-04-17 06:33:14'),
(19, 6, 'Toán học và thông kê', '2019-04-17 06:35:14', '2019-04-17 06:35:14'),
(20, 1, 'Máy tính', '2019-04-17 06:35:39', '2019-04-17 06:35:39'),
(21, 1, 'Công nghệ thông tin', '2019-04-17 06:35:53', '2019-04-17 06:35:53'),
(22, 1, 'Công nghệ kỹ thuật', '2019-04-17 06:36:21', '2019-04-17 06:36:21'),
(23, 1, 'Kỹ thuật', '2019-04-17 06:37:01', '2019-04-17 06:37:01');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_04_15_085826_question', 2),
(4, '2019_04_15_085951_codes', 2),
(5, '2019_04_15_090039_majors', 2),
(6, '2019_04_25_211220_add_columns_to_users_table', 3),
(7, '2019_04_25_222158_alter_users_table', 4);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE `questions` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `code_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`id`, `title`, `code_id`, `created_at`, `updated_at`) VALUES
(1, 'Tôi có tính tự lập', 1, '2019-04-15 10:54:45', '2019-04-15 10:54:45'),
(2, 'Tôi suy nghĩ thực tế', 1, '2019-04-15 10:55:22', '2019-04-15 10:55:22'),
(3, 'Tôi là người dễ thích nghi với môi trường mới', 1, '2019-04-15 10:56:00', '2019-04-15 10:56:00'),
(4, 'Tôi có thể vận hành, điều khiển các máy móc thiết bị', 1, '2019-04-15 10:56:44', '2019-04-15 10:56:44'),
(5, 'Tôi làm các công việc thủ công như gấp giấy, đan, móc', 1, '2019-04-15 10:57:54', '2019-04-15 10:57:54'),
(6, 'Tôi thích tiếp xúc với thiên nhiên, động vật, cây cỏ', 1, '2019-04-15 10:58:08', '2019-04-15 10:58:08'),
(7, 'Tôi thích những công việc sử dụng tay chân hơn là trí óc', 1, '2019-04-15 10:58:20', '2019-04-15 10:58:20'),
(8, 'Tôi thích những công việc thấy ngay kết quả', 1, '2019-04-15 10:58:29', '2019-04-15 10:58:29'),
(9, 'Tôi thích làm việc ngoài trời hơn là trong phòng học, văn phòng', 1, '2019-04-15 10:58:43', '2019-04-15 10:58:43'),
(10, 'Tôi có tìm hiểu khám phá nhiều vấn đề mới', 2, '2019-04-15 10:58:57', '2019-04-15 10:58:57'),
(11, 'Tôi có khả năng phân tích vấn đề', 2, '2019-04-15 10:59:07', '2019-04-15 10:59:07'),
(12, 'Tôi biết suy nghĩ một cách mạch lạc, chặt chẽ', 2, '2019-04-15 10:59:18', '2019-04-15 10:59:18'),
(13, 'Tôi thích thực hiện các thí nghiệm hay nghiên cứu', 2, '2019-04-15 10:59:27', '2019-04-15 10:59:27'),
(14, 'Tôi có khả năng tổng hợp, khái quát, suy đoán những vấn đề', 2, '2019-04-15 10:59:39', '2019-04-15 10:59:39'),
(15, 'Tôi thích những hoạt động điều tra, phân loại, kiểm tra, đánh giá', 2, '2019-04-15 10:59:51', '2019-04-15 10:59:51'),
(16, 'Tôi tự tổ chức công việc mình phái làm', 2, '2019-04-15 11:00:10', '2019-04-15 11:00:10'),
(17, 'Tôi thích suy nghĩ về những vấn đề phức tạp, làm những công việc phức tạp', 2, '2019-04-15 11:00:20', '2019-04-15 11:00:20'),
(18, 'Tôi có khả năng giải quyết các vấn đề', 2, '2019-04-15 11:00:29', '2019-04-15 11:00:29'),
(19, 'Tôi là người dễ xúc động', 3, '2019-04-15 11:01:30', '2019-04-15 11:01:30'),
(20, 'Tôi có óc tưởng tượng phong phú', 3, '2019-04-15 11:01:38', '2019-04-15 11:01:38'),
(21, 'Tôi thích sự tự do, không theo những quy định , quy tắc', 3, '2019-04-15 11:01:46', '2019-04-15 11:01:46'),
(22, 'Tôi có khả năng thuyết trình, diễn xuất', 3, '2019-04-15 11:01:55', '2019-04-15 11:01:55'),
(23, 'Tôi có thể chụp hình hoặc vẽ tranh, trang trí, điêu khắc', 3, '2019-04-15 11:02:03', '2019-04-15 11:02:03'),
(24, 'Tôi có năng khiếu âm nhạc', 3, '2019-04-15 11:02:10', '2019-04-15 11:02:10'),
(25, 'Tôi có khả năng viết, trình bày những ý tưởng của mình', 3, '2019-04-15 11:02:19', '2019-04-15 11:02:19'),
(26, 'Tôi thích làm những công việc mới, những công việc đòi hỏi sự sáng tạo', 3, '2019-04-15 11:02:29', '2019-04-15 11:02:29'),
(27, 'Tôi thoải mái bộc lộ những ý thích', 3, '2019-04-15 11:02:38', '2019-04-15 11:02:38'),
(28, 'Tôi là người thân thiện, hay giúp đỡ người khác', 4, '2019-04-15 11:03:03', '2019-04-15 11:03:03'),
(29, 'Tôi thích gặp gỡ, làm việc với con người', 4, '2019-04-15 11:03:10', '2019-04-15 11:03:10'),
(30, 'Tôi là người lịch sự, tử tế', 4, '2019-04-15 11:03:21', '2019-04-15 11:03:21'),
(31, 'Tôi thích khuyên bảo, huấn luyện hay giảng giái cho người khác', 4, '2019-04-15 11:03:44', '2019-04-15 11:03:44'),
(32, 'Tôi là người biệt lắng nghe', 4, '2019-04-15 11:03:52', '2019-04-15 11:03:52'),
(33, 'Tôi thích các hoạt động chăm sóc sức khỏe của bản thân và người khác', 4, '2019-04-15 11:04:01', '2019-04-15 11:04:01'),
(34, 'Tôi thích các hoạt độngvì mục tiêu chung của công đồng, xã hội', 4, '2019-04-15 11:04:09', '2019-04-15 11:04:09'),
(35, 'Tôi mong muốn mình có thể đóng góp để xã hội tốt đẹp hơn', 4, '2019-04-15 11:04:17', '2019-04-15 11:04:17'),
(36, 'Tôi mong muốn mình có thể đóng góp để xã hội tốt đẹp hơn', 4, '2019-04-15 11:04:25', '2019-04-15 11:04:25'),
(37, 'Tôi là người có tính phiêu lưu, mạo hiểm', 5, '2019-04-15 11:04:36', '2019-04-15 11:04:36'),
(38, 'Tôi có tính quyết đoán', 5, '2019-04-15 11:04:44', '2019-04-15 11:04:44'),
(39, 'Tôi là người năng động', 5, '2019-04-15 11:04:51', '2019-04-15 11:04:51'),
(40, 'Tôi có khả năng diễn đạt, tranh luận, và thuyết phục người khác', 5, '2019-04-15 11:04:58', '2019-04-15 11:04:58'),
(41, 'Tôi thíc các việc quản lý, đánh giá', 5, '2019-04-15 11:05:08', '2019-04-15 11:05:08'),
(42, 'Tôi thường đặt ra các mục tiêu, kế hoạch trong cuộc sống', 5, '2019-04-15 11:05:16', '2019-04-15 11:05:16'),
(43, 'Tôi thích gây ảnh hưởng đến người khác', 5, '2019-04-15 11:05:24', '2019-04-15 11:05:24'),
(44, 'Tôi là người thích cạnh tranh, và muốn mình giói hơn người khác', 5, '2019-04-15 11:05:33', '2019-04-15 11:05:33'),
(45, 'Tôi muốn người khác phải kính trọng, nể phục tôi', 5, '2019-04-15 11:05:41', '2019-04-15 11:05:41'),
(46, 'Tôi là người có đầu óc sắp xếp, tổ chức', 6, '2019-04-15 11:05:50', '2019-04-15 11:07:10'),
(47, 'Tôi có tính cẩn thận', 6, '2019-04-15 11:09:11', '2019-04-15 11:09:11'),
(48, 'Tôi là người chu đáo, chính xác và đáng tin cậy', 6, '2019-04-15 11:09:18', '2019-04-15 11:09:18'),
(49, 'Tôi thích công việc tính toán sổ sách, ghi chép số liệu', 6, '2019-04-15 11:09:25', '2019-04-15 11:09:25'),
(50, 'Tôi thíc các công việc lưu trữ, phân loại, cập nhất thông tin', 6, '2019-04-15 11:09:33', '2019-04-15 11:09:33'),
(51, 'Tôi thường đặt ra những mục tiêu, kế hoạch trong cuộc sống', 6, '2019-04-15 11:09:41', '2019-04-15 11:09:41'),
(52, 'Tôi thích dự kiến các khoản thu chi', 6, '2019-04-15 11:09:50', '2019-04-15 11:09:50'),
(53, 'Tôi thích lập thời khóa biểu, sắp xếp lịch làm việc', 6, '2019-04-15 11:09:59', '2019-04-15 11:09:59'),
(54, 'Tôi thích làm việc với các con số, làm việc theo hướng dẫn, quy trình', 6, '2019-04-15 11:10:08', '2019-04-15 11:10:08');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `gender` tinyint(1) NOT NULL DEFAULT '1',
  `birthday` date DEFAULT NULL,
  `phone` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `result` text COLLATE utf8mb4_unicode_ci,
  `chosen_codes` text COLLATE utf8mb4_unicode_ci,
  `chosen_majors` text COLLATE utf8mb4_unicode_ci,
  `role` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'student'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `gender`, `birthday`, `phone`, `address`, `result`, `chosen_codes`, `chosen_majors`, `role`) VALUES
(1, 'admin', 'admin@huongnghiep.test', NULL, '$2y$10$LoJnOV8NI/zAGehvk7UD7Oa2ijZ0wi.gs69736Und8DjP7SXQkNLC', NULL, NULL, NULL, 1, NULL, '', NULL, NULL, NULL, NULL, 'admin'),
(2, 'Nguyen Van A', 'hs@test.test', NULL, '$2y$10$s6dWYXggTQXf/nYR.0ZCVOrMldFnjYXJxaygdv7vofjzxSSby3JzS', NULL, '2019-04-25 15:20:43', '2019-04-25 16:56:54', 1, '2019-04-24', '0963456234', '0963456234', '{\"first\":{\"code\":{\"id\":1,\"title\":\"R (Realistic): Th\\u1ef1c t\\u1ebf\",\"description\":\"C\\u00f3 kh\\u1ea3 n\\u0103ng v\\u1ec1 k\\u00fd thu\\u1eadt , c\\u00f4ng ngh\\u1ec7, h\\u1ec7 th\\u1ed1ng, th\\u00edch l\\u00e0m vi\\u1ec7c v\\u1edbi \\u0111\\u1ed3 v\\u1eadt, m\\u00e1y m\\u00f3c, \\u0111\\u1ed9ng th\\u1ef1c v\\u1eadt, th\\u00edch l\\u00e0m c\\u00e1c c\\u00f4ng vi\\u1ec7c ngo\\u00e0i tr\\u1eddi\",\"created_at\":\"2019-04-15 17:31:37\",\"updated_at\":\"2019-04-25 19:48:47\",\"majors\":[{\"id\":20,\"code_id\":1,\"title\":\"M\\u00e1y t\\u00ednh\",\"created_at\":\"2019-04-17 13:35:39\",\"updated_at\":\"2019-04-17 13:35:39\"},{\"id\":21,\"code_id\":1,\"title\":\"C\\u00f4ng ngh\\u1ec7 th\\u00f4ng tin\",\"created_at\":\"2019-04-17 13:35:53\",\"updated_at\":\"2019-04-17 13:35:53\"},{\"id\":22,\"code_id\":1,\"title\":\"C\\u00f4ng ngh\\u1ec7 k\\u1ef9 thu\\u1eadt\",\"created_at\":\"2019-04-17 13:36:21\",\"updated_at\":\"2019-04-17 13:36:21\"},{\"id\":23,\"code_id\":1,\"title\":\"K\\u1ef9 thu\\u1eadt\",\"created_at\":\"2019-04-17 13:37:01\",\"updated_at\":\"2019-04-17 13:37:01\"}]},\"total\":13,\"items\":{\"1\":\"2\",\"2\":\"3\",\"3\":\"2\",\"4\":\"1\",\"5\":\"1\",\"6\":\"1\",\"7\":\"1\",\"8\":\"1\",\"9\":\"1\"},\"countArr\":{\"5\":0,\"4\":0,\"3\":1,\"2\":2,\"1\":6},\"sortNum\":1300126},\"second\":{\"code\":{\"id\":6,\"title\":\"C (Conventional): T\\u1eadp qu\\u00e1n\",\"description\":\"C\\u00f3 kh\\u1ea3 n\\u0103ng v\\u1ec1 s\\u1ed1 h\\u1ecdc, th\\u00edch th\\u1ef1c hi\\u1ec7n nh\\u1eefng c\\u00f4ng vi\\u1ec7c chi ti\\u1ebft, th\\u00edch l\\u00e0m vi\\u1ec7c v\\u1edbi nh\\u1eefng s\\u1ed1 li\\u1ec7u, theo ch\\u1ec9 d\\u1eabn c\\u1ec9a ng\\u01b0\\u1eddi kh\\u00e1c ho\\u1eb7c c\\u00e1c c\\u00f4ng vi\\u1ec7c v\\u0103n ph\\u00f2ng\",\"created_at\":\"2019-04-15 17:37:54\",\"updated_at\":\"2019-04-25 19:54:13\",\"majors\":[{\"id\":9,\"code_id\":6,\"title\":\"K\\u1ebf to\\u00e1n - Ki\\u1ec3m to\\u00e1n\",\"created_at\":\"2019-04-17 13:19:06\",\"updated_at\":\"2019-04-17 13:19:06\"},{\"id\":17,\"code_id\":6,\"title\":\"V\\u0103n th\\u01b0 -L\\u01b0u tr\\u1eef - B\\u1ea3o t\\u00e0ng\",\"created_at\":\"2019-04-17 13:32:29\",\"updated_at\":\"2019-04-17 13:32:29\"},{\"id\":19,\"code_id\":6,\"title\":\"To\\u00e1n h\\u1ecdc v\\u00e0 th\\u00f4ng k\\u00ea\",\"created_at\":\"2019-04-17 13:35:14\",\"updated_at\":\"2019-04-17 13:35:14\"}]},\"total\":12,\"items\":{\"46\":\"1\",\"47\":\"1\",\"48\":\"1\",\"49\":\"1\",\"50\":\"1\",\"51\":\"1\",\"52\":\"1\",\"53\":\"4\",\"54\":\"1\"},\"countArr\":{\"5\":0,\"4\":1,\"3\":0,\"2\":0,\"1\":8},\"sortNum\":1201008},\"results\":[{\"code\":{\"id\":1,\"title\":\"R (Realistic): Th\\u1ef1c t\\u1ebf\",\"description\":\"C\\u00f3 kh\\u1ea3 n\\u0103ng v\\u1ec1 k\\u00fd thu\\u1eadt , c\\u00f4ng ngh\\u1ec7, h\\u1ec7 th\\u1ed1ng, th\\u00edch l\\u00e0m vi\\u1ec7c v\\u1edbi \\u0111\\u1ed3 v\\u1eadt, m\\u00e1y m\\u00f3c, \\u0111\\u1ed9ng th\\u1ef1c v\\u1eadt, th\\u00edch l\\u00e0m c\\u00e1c c\\u00f4ng vi\\u1ec7c ngo\\u00e0i tr\\u1eddi\",\"created_at\":\"2019-04-15 17:31:37\",\"updated_at\":\"2019-04-25 19:48:47\",\"majors\":[{\"id\":20,\"code_id\":1,\"title\":\"M\\u00e1y t\\u00ednh\",\"created_at\":\"2019-04-17 13:35:39\",\"updated_at\":\"2019-04-17 13:35:39\"},{\"id\":21,\"code_id\":1,\"title\":\"C\\u00f4ng ngh\\u1ec7 th\\u00f4ng tin\",\"created_at\":\"2019-04-17 13:35:53\",\"updated_at\":\"2019-04-17 13:35:53\"},{\"id\":22,\"code_id\":1,\"title\":\"C\\u00f4ng ngh\\u1ec7 k\\u1ef9 thu\\u1eadt\",\"created_at\":\"2019-04-17 13:36:21\",\"updated_at\":\"2019-04-17 13:36:21\"},{\"id\":23,\"code_id\":1,\"title\":\"K\\u1ef9 thu\\u1eadt\",\"created_at\":\"2019-04-17 13:37:01\",\"updated_at\":\"2019-04-17 13:37:01\"}]},\"total\":13,\"items\":{\"1\":\"2\",\"2\":\"3\",\"3\":\"2\",\"4\":\"1\",\"5\":\"1\",\"6\":\"1\",\"7\":\"1\",\"8\":\"1\",\"9\":\"1\"},\"countArr\":{\"5\":0,\"4\":0,\"3\":1,\"2\":2,\"1\":6},\"sortNum\":1300126},{\"code\":{\"id\":2,\"title\":\"I (Investigative): \\u0110i\\u1ec1u tra\",\"description\":\"C\\u00f3 kh\\u1ea3 n\\u0103ng v\\u1ec1 quan s\\u00e1t, kh\\u00e1m ph\\u00e1, nghi\\u00ean c\\u1ee9u, ph\\u00e2n t\\u00edch \\u0111\\u00e1nh gi\\u00e1 v\\u00e0 gi\\u1ea3i quy\\u1ebft c\\u00e1c v\\u1ea5n \\u0111\\u1ec1\",\"created_at\":\"2019-04-15 17:32:32\",\"updated_at\":\"2019-04-25 19:49:37\",\"majors\":[{\"id\":10,\"code_id\":2,\"title\":\"Khoa h\\u1ecdc tr\\u00e1i \\u0111\\u1ea5t\",\"created_at\":\"2019-04-17 13:21:15\",\"updated_at\":\"2019-04-17 13:21:15\"},{\"id\":11,\"code_id\":2,\"title\":\"Khoa h\\u1ecdc v\\u1eadt ch\\u1ea5t\",\"created_at\":\"2019-04-17 13:21:23\",\"updated_at\":\"2019-04-17 13:21:23\"},{\"id\":12,\"code_id\":2,\"title\":\"Khoa h\\u1ecdc m\\u00f4i tr\\u01b0\\u1eddng\",\"created_at\":\"2019-04-17 13:21:40\",\"updated_at\":\"2019-04-17 13:21:40\"},{\"id\":13,\"code_id\":2,\"title\":\"Khoa h\\u1ecdc s\\u1ef1 s\\u1ed1ng\",\"created_at\":\"2019-04-17 13:21:49\",\"updated_at\":\"2019-04-17 13:21:49\"}]},\"total\":9,\"items\":{\"10\":\"1\",\"11\":\"1\",\"12\":\"1\",\"13\":\"1\",\"14\":\"1\",\"15\":\"1\",\"16\":\"1\",\"17\":\"1\",\"18\":\"1\"},\"countArr\":{\"5\":0,\"4\":0,\"3\":0,\"2\":0,\"1\":9},\"sortNum\":900009},{\"code\":{\"id\":3,\"title\":\"A (Artistic): Ngh\\u1ec7 s\\u0129\",\"description\":\"C\\u00f3 kh\\u1ea3 n\\u0103ng v\\u1ec1 ngh\\u1ec7 thu\\u1eadt, kh\\u1ea3 n\\u0103ng v\\u1ec1 tr\\u1ef1c gi\\u00e1c, kh\\u1ea3 n\\u0103ng t\\u01b0\\u1edfng t\\u01b0\\u1ee3ng cao, th\\u00edch l\\u00e0m vi\\u1ec7c trong c\\u00e1c m\\u00f4i tr\\u01b0\\u1eddng mang t\\u00ednh ng\\u1eabu h\\u1ee9ng, kh\\u00f4ng khu\\u00f4n m\\u1eabu\",\"created_at\":\"2019-04-15 17:33:01\",\"updated_at\":\"2019-04-25 19:50:41\",\"majors\":[{\"id\":3,\"code_id\":3,\"title\":\"Ngh\\u1ec7 thu\\u1eadt tr\\u00ecnh di\\u1ec5n\",\"created_at\":\"2019-04-17 13:15:34\",\"updated_at\":\"2019-04-17 13:15:34\"},{\"id\":4,\"code_id\":3,\"title\":\"M\\u1ef9 thu\\u1eadt \\u1ee9ng d\\u1ee5ng\",\"created_at\":\"2019-04-17 13:15:59\",\"updated_at\":\"2019-04-17 13:15:59\"},{\"id\":14,\"code_id\":3,\"title\":\"M\\u1ef9 thu\\u1eadt\",\"created_at\":\"2019-04-17 13:24:33\",\"updated_at\":\"2019-04-17 13:24:33\"}]},\"total\":9,\"items\":{\"19\":\"1\",\"20\":\"1\",\"21\":\"1\",\"22\":\"1\",\"23\":\"1\",\"24\":\"1\",\"25\":\"1\",\"26\":\"1\",\"27\":\"1\"},\"countArr\":{\"5\":0,\"4\":0,\"3\":0,\"2\":0,\"1\":9},\"sortNum\":900009},{\"code\":{\"id\":4,\"title\":\"S (Social): X\\u00e3 h\\u1ed9i\",\"description\":\"C\\u00f3 kh\\u1ea3 n\\u0103ng v\\u1ec1 ng\\u00f4n ng\\u1eef, gi\\u1ea3ng gi\\u1ea3i, th\\u00edch l\\u00e0m nh\\u1eefng vi\\u1ec7c nh\\u01b0 gi\\u1ea3ng gi\\u1ea3i, cung c\\u1ea5p th\\u00f4ng tin, s\\u1ef1 ch\\u0103m s\\u00f3c, gi\\u00fap \\u0111\\u1ee1 ho\\u1eb7c hu\\u1ea5n luy\\u1ec7n cho ng\\u01b0\\u1eddi kh\\u00e1c\",\"created_at\":\"2019-04-15 17:33:24\",\"updated_at\":\"2019-04-25 19:52:06\",\"majors\":[{\"id\":2,\"code_id\":4,\"title\":\"\\u0110\\u00e0o t\\u1ea1o gi\\u00e1o vi\\u00ean\",\"created_at\":\"2019-04-15 18:40:13\",\"updated_at\":\"2019-04-15 18:40:13\"},{\"id\":5,\"code_id\":4,\"title\":\"Khoa h\\u1ecdc ch\\u00ednh tr\\u1ecb\",\"created_at\":\"2019-04-17 13:17:48\",\"updated_at\":\"2019-04-17 13:17:48\"},{\"id\":6,\"code_id\":4,\"title\":\"X\\u00e3 h\\u1ed9i h\\u1ecdc v\\u00e0 nh\\u00e2n h\\u1ecdc\",\"created_at\":\"2019-04-17 13:18:00\",\"updated_at\":\"2019-04-17 13:18:00\"},{\"id\":7,\"code_id\":4,\"title\":\"T\\u00e2m l\\u00fd h\\u1ecdc\",\"created_at\":\"2019-04-17 13:18:15\",\"updated_at\":\"2019-04-17 13:18:15\"},{\"id\":15,\"code_id\":4,\"title\":\"B\\u00e1o ch\\u00ed v\\u00e0 truy\\u1ec1n th\\u00f4ng\",\"created_at\":\"2019-04-17 13:28:25\",\"updated_at\":\"2019-04-17 13:28:25\"},{\"id\":18,\"code_id\":4,\"title\":\"Lu\\u1eadt\",\"created_at\":\"2019-04-17 13:33:14\",\"updated_at\":\"2019-04-17 13:33:14\"}]},\"total\":9,\"items\":{\"28\":\"1\",\"29\":\"1\",\"30\":\"1\",\"31\":\"1\",\"32\":\"1\",\"33\":\"1\",\"34\":\"1\",\"35\":\"1\",\"36\":\"1\"},\"countArr\":{\"5\":0,\"4\":0,\"3\":0,\"2\":0,\"1\":9},\"sortNum\":900009},{\"code\":{\"id\":5,\"title\":\"E (Enterprising): D\\u00e1m ngh\\u0129 d\\u00e1m l\\u00e0m\",\"description\":\"C\\u00f3 kh\\u1ea3 n\\u0103ng v\\u1ec1 kinh doanh, m\\u1ea1nh b\\u1ea1o, d\\u00e1m ngh\\u0129 d\\u00e1m l\\u00e0m, c\\u00f3 th\\u1ec3 g\\u00e2y \\u1ea3nh h\\u01b0\\u1edfng, thuy\\u1ebft ph\\u1ee5c ng\\u01b0\\u1eddi kh\\u00e1c, c\\u00f3 kh\\u1ea3 n\\u0103ng qu\\u1ea3n l\\u00fd\",\"created_at\":\"2019-04-15 17:37:18\",\"updated_at\":\"2019-04-25 19:53:01\",\"majors\":[{\"id\":8,\"code_id\":5,\"title\":\"Qu\\u1ea3n tr\\u1ecb - Qu\\u1ea3n l\\u00fd\",\"created_at\":\"2019-04-17 13:18:48\",\"updated_at\":\"2019-04-17 13:18:48\"},{\"id\":16,\"code_id\":5,\"title\":\"Kinh doanh\",\"created_at\":\"2019-04-17 13:28:52\",\"updated_at\":\"2019-04-17 13:28:52\"}]},\"total\":9,\"items\":{\"37\":\"1\",\"38\":\"1\",\"39\":\"1\",\"40\":\"1\",\"41\":\"1\",\"42\":\"1\",\"43\":\"1\",\"44\":\"1\",\"45\":\"1\"},\"countArr\":{\"5\":0,\"4\":0,\"3\":0,\"2\":0,\"1\":9},\"sortNum\":900009},{\"code\":{\"id\":6,\"title\":\"C (Conventional): T\\u1eadp qu\\u00e1n\",\"description\":\"C\\u00f3 kh\\u1ea3 n\\u0103ng v\\u1ec1 s\\u1ed1 h\\u1ecdc, th\\u00edch th\\u1ef1c hi\\u1ec7n nh\\u1eefng c\\u00f4ng vi\\u1ec7c chi ti\\u1ebft, th\\u00edch l\\u00e0m vi\\u1ec7c v\\u1edbi nh\\u1eefng s\\u1ed1 li\\u1ec7u, theo ch\\u1ec9 d\\u1eabn c\\u1ec9a ng\\u01b0\\u1eddi kh\\u00e1c ho\\u1eb7c c\\u00e1c c\\u00f4ng vi\\u1ec7c v\\u0103n ph\\u00f2ng\",\"created_at\":\"2019-04-15 17:37:54\",\"updated_at\":\"2019-04-25 19:54:13\",\"majors\":[{\"id\":9,\"code_id\":6,\"title\":\"K\\u1ebf to\\u00e1n - Ki\\u1ec3m to\\u00e1n\",\"created_at\":\"2019-04-17 13:19:06\",\"updated_at\":\"2019-04-17 13:19:06\"},{\"id\":17,\"code_id\":6,\"title\":\"V\\u0103n th\\u01b0 -L\\u01b0u tr\\u1eef - B\\u1ea3o t\\u00e0ng\",\"created_at\":\"2019-04-17 13:32:29\",\"updated_at\":\"2019-04-17 13:32:29\"},{\"id\":19,\"code_id\":6,\"title\":\"To\\u00e1n h\\u1ecdc v\\u00e0 th\\u00f4ng k\\u00ea\",\"created_at\":\"2019-04-17 13:35:14\",\"updated_at\":\"2019-04-17 13:35:14\"}]},\"total\":12,\"items\":{\"46\":\"1\",\"47\":\"1\",\"48\":\"1\",\"49\":\"1\",\"50\":\"1\",\"51\":\"1\",\"52\":\"1\",\"53\":\"4\",\"54\":\"1\"},\"countArr\":{\"5\":0,\"4\":1,\"3\":0,\"2\":0,\"1\":8},\"sortNum\":1201008}]}', '[\"1\",\"2\"]', '[\"18\",\"13\"]', 'student');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `codes`
--
ALTER TABLE `codes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `majors`
--
ALTER TABLE `majors`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `majors_title_unique` (`title`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `codes`
--
ALTER TABLE `codes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `majors`
--
ALTER TABLE `majors`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `questions`
--
ALTER TABLE `questions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
