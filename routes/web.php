<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();



Route::middleware(['auth'])->group(function () {
    Route::get('profile', 'HomeController@profile')->name('profile');
    Route::get('/', 'HomeController@index')->name('home');
    Route::post('/', 'HomeController@result');
    Route::resource('questions', 'QuestionController');
    Route::resource('codes', 'CodeController');
    Route::resource('majors', 'MajorController');
});
